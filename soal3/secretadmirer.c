#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define KEY "secret" // Password untuk membuka file

static const char *dirpath = "/etc"; // Path untuk Mount

// Fungsi untuk melakukan enkripsi Base64
char* base64_encode(const unsigned char* input, int length) {
    BIO *bmem, *b64;
    BUF_MEM *bptr;

    b64 = BIO_new(BIO_f_base64());
    bmem = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, bmem);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_write(b64, input, length);
    BIO_flush(b64);
    BIO_get_mem_ptr(b64, &bptr);

    char *buff = (char*)malloc(bptr->length);
    memcpy(buff, bptr->data, bptr->length - 1);
    buff[bptr->length - 1] = '\0';

    BIO_free_all(b64);

    return buff;
}

// Fungsi untuk melakukan enkripsi Base64 pada file atau direktori
void encode_base64(char *path, int is_directory) {
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;

    SHA256_Init(&sha256);

    if (is_directory)
        SHA256_Update(&sha256, path, strlen(path));
    else {
        FILE *file = fopen(path, "rb");
        if (!file)
            return;

        int bytes;
        unsigned char data[1024];
        while ((bytes = fread(data, 1, sizeof(data), file)) != 0) {
            SHA256_Update(&sha256, data, bytes);
        }
        fclose(file);
    }

    SHA256_Final(hash, &sha256);

    char *encoded = base64_encode(hash, sizeof(hash));

    for (int i = 0; encoded[i]; i++) {
        if (is_directory)
            encoded[i] = toupper(encoded[i]);
        else
            encoded[i] = tolower(encoded[i]);
    }

    if (strlen(encoded) <= 4) {
        // Ubah nama menggunakan data format binary dari ASCII code
        char new_name[256];
        for (int i = 0; encoded[i]; i++) {
            sprintf(new_name + (i * 2), "%02x", encoded[i]);
        }
        rename(path, new_name);
    } else {
        char new_path[256];
        sprintf(new_path, "%s/%s", dirpath, encoded);
        rename(path, new_path);
    }

    free(encoded);
}

// Fungsi untuk mendapatkan path yang akan digunakan dalam sistem FUSE
static void get_real_path(char fpath[256], const char *path) {
    strcpy(fpath, dirpath);
    strcat(fpath, path);
}

// Fungsi untuk melakukan enkripsi Base64 pada direktori dan file
static int encode_and_rename(const char *path, int is_directory) {
    if (path && (path[1] == '.') && (path[2] == 'd')) {
        // Menyembunyikan direktori .dotfiles
        return 1;
    }

    if (path[1] != 'L' && path[1] != 'U' && path[1] != 'T' && path[1] != 'H') {
        // Tidak melakukan enkripsi pada direktori atau file yang tidak memenuhi syarat
        return 0;
    }

    char fpath[256];
    get_real_path(fpath, path);

    encode_base64(fpath, is_directory);

    return 0;
}

// Fungsi untuk mengubah lowercase menjadi uppercase pada path
void to_uppercase(char *str) {
    while (*str) {
        *str = toupper((unsigned char)*str);
        str++;
    }
}

// Fungsi untuk memeriksa apakah path adalah direktori
int is_directory(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) == 0) {
        return S_ISDIR(path_stat.st_mode);
    }
    return 0;
}

// Fungsi untuk melakukan enkripsi saat membuka file
static int encfs_open(const char *path, struct fuse_file_info *fi) {
    if (strcmp(path, "/") == 0) {
        return 0;
    }

    if (is_directory(path)) {
        return -EISDIR;
    }

    char fpath[256];
    get_real_path(fpath, path);

    int res = open(fpath, fi->flags);
    if (res == -1) {
        return -errno;
    }

    return res;
}

// Fungsi untuk membaca isi file dan melakukan enkripsi saat membaca
static int encfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    if (strcmp(path, "/") == 0) {
        return 0;
    }

    if (is_directory(path)) {
        return -EISDIR;
    }

    char fpath[256];
    get_real_path(fpath, path);

    int res = pread(fi->fh, buf, size, offset);
    if (res == -1) {
        return -errno;
    }

    // Minta password sebelum membaca file
    char password[256];
    printf("Masukkan password: ");
    fgets(password, sizeof(password), stdin);
    password[strcspn(password, "\n")] = '\0';

    if (strcmp(password, KEY) != 0) {
        memset(buf, 0, size);
        return -EACCES;
    }

    return res;
}

// Fungsi untuk melakukan enkripsi saat membuat direktori
static int encfs_mkdir(const char *path, mode_t mode) {
    if (is_directory(path)) {
        return -EEXIST;
    }

    char fpath[256];
    get_real_path(fpath, path);

    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    encode_and_rename(path, 1);

    return 0;
}

// Fungsi untuk melakukan enkripsi saat rename file atau direktori
static int encfs_rename(const char *from, const char *to) {
    char ffrom[256], fto[256];
    get_real_path(ffrom, from);
    get_real_path(fto, to);

    int res = rename(ffrom, fto);
    if (res == -1) {
        return -errno;
    }

    if (is_directory(to)) {
        encode_and_rename(to, 1);
    } else {
        encode_and_rename(to, 0);
    }

    return 0;
}

// Fungsi untuk mengimplementasikan FUSE operations
static struct fuse_operations encfs_oper = {
    .getattr = encfs_getattr,
    .readdir = encfs_readdir,
    .open = encfs_open,
    .read = encfs_read,
    .mkdir = encfs_mkdir,
    .rename = encfs_rename,
};

int main(int argc, char *argv[]) {
    umask(0);

    return fuse_main(argc, argv, &encfs_oper, NULL);
}
