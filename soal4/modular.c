#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Fungsi untuk membagi file menjadi file-file chunk
void splitFile(const char* filePath) {
    FILE* inputFile = fopen(filePath, "rb");
    if (inputFile == NULL) {
        printf("File tidak ditemukan!\n");
        return;
    }

    fseek(inputFile, 0, SEEK_END);
    long fileSize = ftell(inputFile);
    rewind(inputFile);

    int chunkSize = 1024; // 1 kilobyte
    char chunk[chunkSize];
    int chunkCount = fileSize / chunkSize;
    int remainingBytes = fileSize % chunkSize;

    char outputFileName[256];
    strncpy(outputFileName, filePath, 256);
    strcat(outputFileName, ".000");

    FILE* outputFile = fopen(outputFileName, "wb");

    for (int i = 0; i < chunkCount; i++) {
        fread(chunk, chunkSize, 1, inputFile);
        fwrite(chunk, chunkSize, 1, outputFile);
        sprintf(outputFileName, "%s.%03d", filePath, i + 1);
        outputFile = fopen(outputFileName, "wb");
    }

    if (remainingBytes > 0) {
        fread(chunk, remainingBytes, 1, inputFile);
        fwrite(chunk, remainingBytes, 1, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("File tersplit!\n");
}

// Fungsi untuk mencatat kegiatan ke dalam sistem log
void logActivity(const char* level, const char* command, const char* description) {
    time_t currentTime;
    struct tm* timeInfo;
    char timestamp[20];
    time(&currentTime);
    timeInfo = localtime(&currentTime);
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", timeInfo);

    printf("[%s]::%s::%s::%s\n", level, timestamp, command, description);

    // Menulis log ke file fs_module.log
    FILE* logFile = fopen("/home/user/fs_module.log", "a");
    if (logFile == NULL) {
        printf("Failed to open log file!\n");
        return;
    }

    fprintf(logFile, "[%s]::%s::%s::%s\n", level, timestamp, command, description);

    fclose(logFile);
}

int main() {
    // fungsi splitFile()
    const char* filePath = "/pathnyabro/filenya.txt";
    splitFile(filePath);

    // fungsi logActivity()
    const char* level = "REPORT";
    const char* command = "RENAME";
    const char* description = "/bsi23/bagong.jpg::/bsi23/bagong.jpeg";
    logActivity(level, command, description);

    return 0;
}
