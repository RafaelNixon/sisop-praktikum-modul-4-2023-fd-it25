 #include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <csv.h>


#define MAX_PLAYERS 666

typedef struct {
    char name[50];
    char club[50];
    int age;
    int potential;
    char photo_url[100];
} Player;

int main() {
	SoalA();
	SoalB()
	return 0;
}

int SoalA() {
	system("pip install kaggle");

	// Perintah untuk mendownload dataset dari Kaggle
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");
	//system("~/.local/bin/kaggle datasets download -d bryanb/fifa-player-stats-database");
	// Perintah untuk mengekstrak file zip
	system("unzip fifa-player-stats-database.zip");
}

int SoalB() {
	FILE *file = fopen("FIFA23_official_data.csv", "r");
	if (!file) {
	printf("Gagal membuka file CSV.\n");
	return 1;
	}

	csv_parser *parser = csv_parser_new();
	if (!parser) {
	printf("Gagal membuat parser CSV.\n");
	fclose(file);
	return 1;
	}

	csv_set_skip_lines(parser, 1);

	Player players[MAX_PLAYERS];
	int playerCount = 0;

	char buffer[1024];
	size_t bytesRead;

	while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
		if (csv_parser_parse(parser, buffer, bytesRead, players, &playerCount) != CSV_OK) {
		    printf("Gagal parsing baris CSV.\n");
		    fclose(file);
		    csv_parser_free(parser);
		    return 1;
		}
	}

	fclose(file);
	csv_parser_free(parser);

	// Mencetak data pemain yang memenuhi kriteria
	for (int i = 0; i < playerCount; i++) {
		if (players[i].age < 25 && players[i].potential > 85 && strcmp(players[i].club, "Manchester City") != 0) {
		    printf("Nama: %s\n", players[i].name);
		    printf("Klub: %s\n", players[i].club);
		    printf("Umur: %d\n", players[i].age);
		    printf("Potensi: %d\n", players[i].potential);
		    printf("URL Foto: %s\n", players[i].photo_url);
		    printf("\n");
		}
	}

    return 0;
}

